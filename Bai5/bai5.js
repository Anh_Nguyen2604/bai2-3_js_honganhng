/*
input: nhap so co 2 chu so

todo:
- tao bien nhap so co 2 chu so
- tinh hang don vi: donVi = n % 10
- tinh hang chuc: hangChuc = Math.floor(n/10)%10

ouput: result = donVi + hangChuc  */
/*
var n;
var donVi;
var hangChuc;
var result;

n = 26
donVi = n % 10;
hangChuc = Math.floor(n / 10);
result = hangChuc + donVi;
console("result: ", result);*/


function tinhTong() {
    var n = document.getElementById("txt-so-n").value * 1;
    var donVi = n % 10;
    var hangChuc = Math.floor(n / 10);
    console.log({donVi, hangChuc});
    var result = donVi + hangChuc;
    document.getElementById("result").innerHTML = 
    ` <h2> Tong ky so la: ${result} </h2>`;
}